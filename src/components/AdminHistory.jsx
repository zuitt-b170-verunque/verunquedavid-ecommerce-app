import { Fragment, useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminHistory() {
    const [order, setOrder] = useState([])

    const fetchData = () => {
        fetch(`https://stormy-sierra-90274.herokuapp.com/api/orders/retrieveall`)
        .then((response) => response.json())
        .then(data => {
            setOrder(data)
        })
    }
    

    const arr = setOrder.map((order) => {

        return (
            <Table striped bordered hover>
                <thead>
                    <tr>
                    <th></th>
                    <th>Total Amount</th>
                    <th>Purchased On</th>
                    </tr>
                </thead>
                <tbody>
                    <tr key={order._id}>
                    <td>{order.totalAmount}</td>
                    <td>{order.purchasedOn}</td>

                    </tr>
                </tbody>
            </Table>
        )
    })
}