import { Fragment, useState, useEffect } from 'react';

import HistoryCard from './HistoryCard.jsx';

export default function UserHistory() {

    const [order, setOrder] = useState([])

    const fetchData = () => {

        fetch(`https://stormy-sierra-90274.herokuapp.com/api/orders/retrieveorder`, {
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            },
        })
        .then(res => res.json())
        .then(data => {

            setOrder(data);
        });
    }

    useEffect(() => {
        fetchData();
    }, []);


   return (
  
       < HistoryCard orderModel ={order}  fetchData={fetchData}></HistoryCard>

   )
}
