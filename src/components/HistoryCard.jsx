import React, {useState, useEffect, Fragment} from 'react';


import { Table} from 'react-bootstrap';




export default function HistoryCard(props) {

    const {orderModel, fetchData} = props;
    const [order, setOrder] = useState([])

    useEffect(() => {

        const arr = orderModel.map(orders => {
            return (
                <tr>
                    <td>{orders._id}</td>
                    <td>{orders.totalAmount}</td>
                    <td>{orders.purchasedOn}</td>
                </tr>
            )
        })
        setOrder(arr)
    }, [orderModel, fetchData])
    
    return (
        <Fragment>
            <Table>
                    <thead>
                        <tr>
                        <th>ID</th>
                        <th>Total Amount</th>
                        <th>Purchased On</th>
                        </tr>
                    </thead>
                    <tbody>
                        {order}
                    </tbody>
            </Table>
        </Fragment>
    )
}
