import React, {useState, useEffect, useContext } from 'react';
import {Navigate} from 'react-router-dom'

// App Imports
import userContext from '../userContext.jsx'

// bootstrap
import {Form, Container, Button}  from 'react-bootstrap'
import Swal from 'sweetalert2'
import { MDBInput } from "mdbreact";

export default function Login(){
    const {user, setUser} = useContext(userContext);


    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isDisabled, setIsDisabled] = useState(true)

    // Disable login button
    useEffect(() => {

        if(email && password !== ''){
            setIsDisabled(false);
        } else {
            setIsDisabled(true);
        }
    }, [email, password])
    
    // Logging in using fetch
    // function

    function login(e) {
        e.preventDefault();

        fetch('https://stormy-sierra-90274.herokuapp.com/api/users/userauthentication', {
            method: 'POST',
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then((response) => response.json())
        .then((response) => {
            console.log(response)
            if(typeof response.access !== 'undefined') {
                localStorage.setItem('token', response.access);
                retrieveUserDetail(response.access)

                Swal.fire({
                    title: "Successfully logged in",
                    icon: 'success',
                    text: "Welcome to Online Tindahan"
                })
            } else {
                Swal.fire({
                    title: "Authentication failed",
                    icon: 'error',
                    text: "Check your login deatails",
                })
            }
        })
        // clear input field
        setEmail('');
        setPassword('');
    }

    const retrieveUserDetail = (token) => {
        fetch(`https://stormy-sierra-90274.herokuapp.com/api/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(response => response.json())
        .then(response => {
            console.log(response)

            setUser({
                id: response._id,
                isAdmin: response.isAdmin
            })
        })
    }


    return(
        (user.id !== null) ?
            <Navigate to={'/'} /> 
            :
        <Container>
            
            <Form onSubmit={login}>
                <Form.Group>
                    <Form.Label>Email:</Form.Label>
                    <Form.Control type = "email" placeholder="email@mail.com" value={email} onChange={(e) => setEmail(e.target.value)}/>
                </Form.Group>

                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type='password' placeholder = 'password' value={password} onChange={(e) => setPassword(e.target.value)}/>
                </Form.Group>

                <Button variant="success" type="submit" disabled={isDisabled}>Login</Button>

            </Form>
        </Container>
    )
}