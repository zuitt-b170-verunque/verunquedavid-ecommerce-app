// Base Imports
import { useState, useEffect, useContext } from 'react';
import {Navigate, useNavigate} from 'react-router-dom';

// App Imports
import userContext from '../userContext.jsx'

// bootstrap
import {Form, Container, Button}  from 'react-bootstrap'
import Swal from 'sweetalert2'

export default function Register() {

    const {user} = useContext(userContext);
    const history = useNavigate();

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState('');

    // Check if values are successfully binded
    console.log(email);
    console.log(password);

    // Function to simulate user registration
    function registerUser(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        fetch(`https://stormy-sierra-90274.herokuapp.com/api/users/checkUser`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            if(data === true){

                Swal.fire({
                    title: 'Duplicate email found',
                    icon: 'error',
                    text: 'Please provide a different email.'   
                });

            } else {

                fetch(`https://stormy-sierra-90274.herokuapp.com/api/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        email: email,
                        password: password
                    })
                })
                .then(res => res.json())
                .then(data => {

                    console.log(data);

                    if(data === true){

                        // Clear input fields
                        setEmail('');
                        setPassword('');


                        Swal.fire({
                            title: 'Registration successful',
                            icon: 'success',
                            text: 'Welcome to Zuitt!'
                        });

                        history.push("/login");



                    } else {

                        Swal.fire({
                            title: 'Something wrong',
                            icon: 'error',
                            text: 'Please try again.'   
                        });

                    };

                })
            };

        })

    }

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if((email !== '' && password!== '')){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [email,password]);

    return (
        (user.id !== null) ?
            <Navigate to="/courses" />
        :
            <Form onSubmit={(e) => registerUser(e)}>


                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
    	                type="email" 
    	                placeholder="Enter email"
                        value={email} 
                        onChange={e => setEmail(e.target.value)}
    	                required
                    />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>


                <Form.Group controlId="password1">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
    	                type="password" 
    	                placeholder="Password"
                        value={password} 
                        onChange={e => setPassword(e.target.value)}
    	                required
                    />
                </Form.Group>

                { isActive ? 
                    <Button variant="primary" type="submit" id="submitBtn">
                        Submit
                    </Button>
                    : 
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                        Submit
                    </Button>
                }
            </Form>
    )
    
}