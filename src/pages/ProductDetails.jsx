import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import userContext from '../userContext.jsx';
import {useCart } from 'react-use-cart'

export default function ProductDetails({productProp, handleClick}) {


	// const { addItem } = useCart();

	const { productId } = useParams();
	const {user} = useContext(userContext)
	const history = useNavigate();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [cart, useCart] = useState([]);


	



	useEffect(()=> {

		console.log(productId);
		fetch(`https://stormy-sierra-90274.herokuapp.com/api/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		});

	}, [productId]);

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Link className="btn btn-outline-info btn-block bg-primary " to="/">Back to Home</Link>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}