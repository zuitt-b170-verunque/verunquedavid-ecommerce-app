import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import userContext from '../userContext.jsx';

export default function Logout() {

    const { unsetUser, setUser } = useContext(userContext);

    unsetUser();
    useEffect(() => {
        setUser({id: null});
    })

    return (
        <Navigate to='/login' />
    )

}