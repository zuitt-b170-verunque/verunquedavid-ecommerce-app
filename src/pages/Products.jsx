// base import
import { Fragment, useEffect, useState, useContext } from 'react';
import userContext from '../userContext'
import UserView from '../components/UserView'
import AdminView from '../components/AdminView'

// Bootstrap Dependency
import Container from 'react-bootstrap/Container'

// Data imports
import products from '../models/ProductModel.js'


// CSS imports
import styles from '../styles/Product.css'

export default function Products({handleClick}){

    const { user } = useContext(userContext);
    const [product, setProduct] = useState([]);

    const fetchData = () => {

        fetch(`https://stormy-sierra-90274.herokuapp.com/api/products/`)
        .then(res => res.json())
        .then(data => {
            setProduct(data);
        });
    }

    useEffect(() => {
        fetchData();
    }, []);
    

    return (
        <Fragment>
            {/* If the user is an admin, show the Admin View. If not, show the regular courses page. */}
            {(user.isAdmin === true)
                ? <AdminView productModel={product} fetchData={fetchData}/>
                : <UserView productModel={product} handleClick={handleClick}/>
            }
        </Fragment>
    )
}
