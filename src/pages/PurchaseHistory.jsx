import { Fragment, useState, useEffect, useContext } from 'react';
import userContext from '../userContext'

import Swal from 'sweetalert2';

// components
import AdminHistory from '../components/AdminHistory.jsx'
import UserHistory from '../components/UserHistory.jsx'

export default function History () {
    
    const { user } = useContext(userContext);

    return (
        <Fragment>
            <UserHistory />
        </Fragment>
    )
}
