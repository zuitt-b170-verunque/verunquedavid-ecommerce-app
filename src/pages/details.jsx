import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import userContext from '../userContext.jsx';


export default function ProductDetails({handleClick}) {


	const { productId } = useParams();
	const {user} = useContext(userContext)
	const history = useNavigate();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const buy = (productId) => {

		fetch(`https://stormy-sierra-90274.herokuapp.com/api/orders/cart`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				productId: productId,
				totalAmount: price

			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if (data === true) {

				Swal.fire({
					title: "Thank you for your purchase",
					icon: 'success',
					text: "View products to buy more."
				});
				history.push("/");

			} else {

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}
		});

	};

	useEffect(()=> {

		console.log(productId);

		fetch(`https://stormy-sierra-90274.herokuapp.com/api/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		});

	}, [productId]);

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							{ user.id !== null ? 
									<Button variant="primary" block onClick={() => buy(productId, price)}>Enroll</Button>
								: 
									<Link className="btn btn-danger btn-block" to="/login">Log in to Buy</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}